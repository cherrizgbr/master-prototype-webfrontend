var request = null;

function submit() {
    request = getJsonDataObj();

    var result = validateInput();

    if (result == true) {
        $.ajax({
            type: "POST",
            dataType: "json",
            url: settings.apiUrl,
            data: request,
            success: showSuccess,
            error: showError
        });

        clearForm();
    }else{
        showWarning(result);
    }
}

function getJsonDataObj() {
    var vorname = $('#input_vorname').val();
    var nachname = $('#input_nachname').val();
    var telefon = $('#input_telefon').val();
    var email = $('#input_email').val();
    var strasse = $('#input_strasse').val();
    var plz = $('#input_plz').val();
    var ort = $('#input_ort').val();

    var reguest = {
        prozess: "kontaktanfrage",
        auftragsDaten: [
            {
                attribute: "vorname",
                value: vorname
            },
            {
                attribute: "nachname",
                value: nachname
            },
            {
                attribute: "telefon",
                value: telefon
            },
            {
                attribute: "email",
                value: email
            },
            {
                attribute: "strasse",
                value: strasse
            },
            {
                attribute: "plz",
                value: plz
            },
            {
                attribute: "ort",
                value: ort
            }
        ]
    }

    return reguest;
}

function validateInput() {
    var vorname = $('#input_vorname').val();
    var nachname = $('#input_nachname').val();
    var telefon = $('#input_telefon').val();
    var email = $('#input_email').val();
    var strasse = $('#input_strasse').val();
    var plz = $('#input_plz').val();
    var ort = $('#input_ort').val();

    var result = "";
    if (vorname.length == 0 || !/^[a-zA-ZäüöÄÜÖß]{3,20}$/.test(vorname)) {
        result += "<br />Der Vorname ist nicht korrekt.";
    }
    if (nachname.length == 0 || !/^[a-zA-ZäüöÄÜÖß]{3,20}$/.test(nachname)) {
        result += "<br />Der Nachname ist nicht korrekt.";
    }
    if (telefon.length == 0 || !/^[0-9\W+-/]{4,30}$/.test(telefon)) {
        result += "<br />Die Telefonnummer ist nicht korrekt.";
    }
    if (email.length == 0 || !/^[0-9a-zA-Z._-]{1,}@[0-9a-zA-Z._-]{1,}$/.test(email)) {
        result += "<br />Die E-Mailadresse ist nicht korrekt.";
    }
    if (strasse.length == 0 || !/^[\Wa-zA-ZäüöÄÜÖß.-]{3,50} [0-9]{1,4}[a-z]{0,1}$/.test(strasse)) {
        result += "<br />Der Strasse ist nicht korrekt.";
    }
    if (plz.length == 0 || !/^[0-9]{5}$/.test(plz)) {
        result += "<br />Der Postleitzahl ist nicht korrekt.";
    }
    if (ort.length == 0 || !/^[\Wa-zA-ZäüöÄÜÖß-]{3,50}$/.test(ort)) {
        result += "<br />Der Ort ist nicht korrekt.";
    }

    if (result.length == 0) {
        return true;
    } else {
        return "Es ist ein Fehler aufgetreten." + result + "<br />Bitte korrigieren Sie die Daten.";
    }
}

function clearForm() {
    if (settings.clearFormAfterSubmit) {
        $('#input_vorname').val("");
        $('#input_nachname').val("");
        $('#input_email').val("");
        $('#input_strasse').val("");
        $('#input_plz').val("");
        $('#input_ort').val("");
    }
}

function showSuccess(response) {
    if ($("#userInfo").is(":visible")) {
        $("#userInfo").hide("slow")
    }

    $("#userInfo").css( "background-color", "yellowgreen" );
    $("#userInfo").html("Kontaktanfrage erfolgreich versendet.<br />Einer unserer Kundenberater wird sich baldmöglichst mit Ihnen in Verbindung setzen.");

    $("#userInfo").show("slow")
    showDevInfos(response);
}

function showWarning(warn){
    if ($("#userInfo").is(":visible")) {
        $("#userInfo").hide("slow")
    }

    $("#userInfo").css( "background-color", "orange" );
    $("#userInfo").html(warn);

    $("#userInfo").show("slow")
}

function showError() {
    if ($("#userInfo").is(":visible")) {
        $("#userInfo").hide("slow")
    }

    $("#userInfo").css( "background-color", "orange" );
    $("#userInfo").html("Es gab einen technischen Fehler. Bitte versuchen Sie es später erneut.");

    $("#userInfo").show("slow")
    showDevError();
}

function showDevError() {
    var response = {status: "Failure", auftragID: -1, prozessID: -1};
    showDevInfos(response);
}

function showDevInfos(response) {
    if (!settings.devMode) {
        return;
    }
    if ($("#devInfo").is(":visible")) {
        $("#devInfo").hide("slow")
    }


    var textIn = "<h3>Gesendet:</h3>";
    textIn += "<b>Prozess: </b>" + request.prozess + "<br />";
    textIn += "<b>Auftragsdaten: </b><br />";
    for (var i = 0; i < request.auftragsDaten.length; i++) {
        textIn += "<b>&nbsp;&nbsp;&nbsp;" + (i + 1) + ": </b>" + request.auftragsDaten[i].attribute + "=" + request.auftragsDaten[i].value + "</br>";
    }
    $("#devInfoSend").html(textIn);

    var textOut = "<h3>Empfangen</h3>";
    textOut += "<b>Status: </b>" + response.status + "<br />";
    textOut += "<b>AuftragsID: </b>" + response.auftragID + "<br />";
    textOut += "<b>ProzessID: </b>" + response.prozessID + "<br />";
    $("#devInfoRecieved").html(textOut);

    $("#devInfo").show("slow")
}
<?php

//Variablen aufbauen
$prozess = $_POST["prozess"];
$daten = $_POST["auftragsDaten"];
$processor = new FormProcessor();

//Verarbeitung durchführen
$auftragID = $processor->createAuftrag($prozess, $daten);
$prozessID = $processor->createProcess($auftragID);
$processor->printResult($auftragID, $prozessID);

/**
 * Class FormProcessor
 *
 * Übernimmt die Verarbeitung für Formulardaten. Dazu wird zunächst ein Auftrag erzeugt
 * und dieser dann an die Process-Engine zur Prozesserzeugung übergeben.
 *
 * @author Frederik Kirsch
 */
class FormProcessor
{

    const AUFTRAG_URL = "auftrag_url";
    const PROZESS_URL = "prozess_url";

    private $settings = null;

    public function __construct()
    {
        $this->settings = parse_ini_file("../settings/settings-server.ini", false);
    }

    public function createAuftrag($prozess, $daten)
    {
        //Übergabeobjekt aufbauen
        $auftrag = json_encode(array("prozess" => $prozess, "auftragsDaten" => $daten));

        //CURL aufruf vorbereiten
        $curl = curl_init($this->settings[FormProcessor::AUFTRAG_URL]);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($curl, CURLOPT_POSTFIELDS, $auftrag);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($auftrag))
        );

        //Aufruf durchführen
        $auftragID = curl_exec($curl);
        curl_close($curl);

        return $auftragID;
    }

    public function createProcess($auftragID)
    {
        if (!is_numeric($auftragID)) {
            return "Error: AuftragsID missing";
        }

        //Übergabeobjekt aufbauen
        $para = json_encode(array("auftragsdaten" => $auftragID));

        //CURL aufruf vorbereiten
        $curl = curl_init($this->settings[FormProcessor::PROZESS_URL]);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_POSTFIELDS, $para);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($para))
        );

        //Aufruf durchführen
        $resultArray = json_decode(curl_exec($curl), true);
        curl_close($curl);

        return $resultArray["id"];
    }

    public function printResult($auftragID, $prozessID)
    {
        //Ergebnisauswertung
        $status = "Failure";
        if (is_numeric($auftragID) && strlen($prozessID) > 2) {
            $status = "Success";
        }

        //Rückgabe an Client
        header('Content-Type: application/json');
        $result = array("status" => $status, "auftragID" => $auftragID, "prozessID" => $prozessID);
        echo json_encode($result);
    }

}
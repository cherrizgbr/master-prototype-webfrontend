# master-prototype-webfrontend #

Dieses Repository ist Teil des Prototyps einer Masterarbeit zum Thema:  
*"Konzeption und prototypische Umsetzung der Integration fachlich getriebener Prozessmodellierung in die Softwareentwicklung unter Verwendung der Business Process Model and Notation 2.0"*  

### Informationen zum Modul ###

Der Web Client nimmt Kontaktanfragen von Anwendern entgegen und führt eine Validierung der Eingaben durch. Bei einer erfolgreichen Anfrage legt der Client einen Auftrag mit den übergebenen Daten an und erzeugt eine neue Prozessinstanz.
![Webfrontend](http://www.cherriz.de/master/images/content/webfrontend.png "Webfrontend")

### Weiterführende Übersicht ###

Der Client ist der im Architekturschaubild gelb dargestellte Web Client. Weiterführende Informationen, Zugang zur Arbeit sowie den anderen Modulen des Prototyps finden Sie auf einer eigenen [Übersichtsseite](www.cherriz.de/master) ([cherriz.de/master](www.cherriz.de/master)).  
![Architekturschaubild Prototyp](http://www.cherriz.de/master/images/content/architektur.png "Architekturschaubild Prototyp")

### Kontakt ###
[www.cherriz.de](www.cherriz.de)  
[www.cherriz.de/master](www.cherriz.de/master)  
[master@cherriz.de](mailto:master@cherriz.de)  


